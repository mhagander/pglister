from .bouncehandler import BounceHandler
from baselib.misc import log


class BounceQueueHandler(object):
    def __init__(self, conn):
        self.conn = conn

    def process_next(self):
        with self.conn.cursor() as curs:
            curs.execute("SELECT id, recipient, sender, dt, messageid, contents FROM bounce_mail WHERE NOT failed ORDER BY id LIMIT 1")
            ll = curs.fetchall()
            if len(ll) == 0:
                # Nothing left, so we're done
                self.conn.rollback()
                return False
            elif len(ll) != 1:
                raise Exception("Should not happen - LIMIT 1 returned more than 1")

            # Else we have an actual email to process
            try:
                id, recipient, sender, dt, messageid, contents = ll[0]
                mail = BounceHandler(self.conn, id, recipient, sender, dt, messageid, contents)
                if mail.process():
                    # Success, so we delete the entry from the queue
                    curs.execute("DELETE FROM bounce_mail WHERE id=%(id)s", {'id': id})
                    log(curs, 0, 'bounce', 'Done processing bounce.', messageid)
                else:
                    # Failure! That's not good...
                    # Flag it as failed and leave it in the queue. That will
                    # also trigger any monitoring that looks at the length
                    # of the queue.
                    curs.execute("UPDATE bounce_mail SET failed='t' WHERE id=%(id)s", {'id': id})
                    log(curs, 2, 'bounce', 'Failed to process bounce!', messageid)
            except Exception as ex:
                print("Exception: %s" % ex)
                self.conn.rollback()
                curs.execute("UPDATE bounce_mail SET failed='t' WHERE id=%(id)s", {'id': id})
                log(curs, 2, 'bounce', 'Exception processing bounce: %s' % ex, messageid)
                self.conn.commit()
            else:
                # New transaction for the next mail
                self.conn.commit()
        return True
