from email.header import decode_header
from email.errors import HeaderParseError

from mailutil.charset import clean_charset


def _maybe_decode(s, charset):
    if isinstance(s, str):
        return s
    return str(s, charset and clean_charset(charset) or 'us-ascii', errors='ignore')


def decode_mime_header(hdr):
    try:
        return " ".join([_maybe_decode(s, charset) for s, charset in decode_header(hdr)])
    except HeaderParseError:
        return str(hdr, 'us-ascii', errors='ignore')
    except Exception:
        raise
        return "Unparsable header"
