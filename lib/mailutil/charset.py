def clean_charset(charset):
    # The world of email has many broken charsets.
    # The rules are copied from the pgarchives project.

    lcharset = charset.lower()
    if lcharset == 'unknown-8bit' or lcharset == 'x-unknown' or lcharset == 'unknown':
        # Special case where we don't know... We'll assume
        # us-ascii and use replacements
        return 'us-ascii'
    if lcharset == '0' or lcharset == 'x-user-defined' or lcharset == '_autodetect_all' or lcharset == 'default_charset':
        # Seriously broken charset definitions, map to us-ascii
        # and throw away the rest with replacements
        return 'us-ascii'
    if lcharset == 'x-gbk':
        # Some MUAs set it to x-gbk, but there is a valid
        # declaratoin as gbk...
        return 'gbk'
    if lcharset == 'iso-8859-8-i':
        # -I is a special logical version, but should be the
        # same charset
        return 'iso-8859-8'
    if lcharset == 'windows-874':
        # This is an alias for iso-8859-11
        return 'iso-8859-11'
    if lcharset == 'iso-88-59-1' or lcharset == 'iso-8858-1':
        # Strange way of saying 8859....
        return 'iso-8859-1'
    if lcharset == 'iso885915':
        return 'iso-8859-15'
    if lcharset == 'iso-latin-2':
        return 'iso-8859-2'
    if lcharset == 'iso-850':
        # Strange spelling of cp850 (windows charset)
        return 'cp850'
    if lcharset == 'koi8r':
        return 'koi8-r'
    if lcharset == 'cp 1252':
        return 'cp1252'
    if lcharset == 'iso-8859-1,iso-8859-2' or lcharset == 'iso-8859-1:utf8:us-ascii':
        # Why did this show up more than once?!
        return 'iso-8859-1'
    if lcharset == 'x-windows-949':
        return 'ms949'
    if lcharset == 'pt_pt' or lcharset == 'de_latin' or lcharset == 'de':
        # This is a locale, and not a charset, but most likely it's this one
        return 'iso-8859-1'
    if lcharset == 'iso-8858-15':
        # How is this a *common* mistake?
        return 'iso-8859-15'
    if lcharset == 'macintosh':
        return 'mac_roman'
    if lcharset == 'cn-big5':
        return 'big5'
    if lcharset == 'x-unicode-2-0-utf-7':
        return 'utf-7'
    if lcharset == 'tscii':
        # No support for this charset :S Map it down to ascii
        # and throw away all the rest. sucks, but we have to
        return 'us-ascii'
    return charset
