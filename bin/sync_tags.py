#!/usr/bin/env python3
#
# sync-tags.py - synchronize tags with upstream for any tagged lists
#
#
# This script is intended to be run from cron at "reasonable intervals",
# if tagged lists are in use.
#
import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from baselib.config import config
from baselib.lists import MailingList


if __name__ == "__main__":
    conn = config.conn('load_subscribers')
    with conn.cursor() as curs:
        curs.execute("SELECT id FROM lists_list WHERE tagged_delivery")
        for id, in curs.fetchall():
            mlist = MailingList.get_by_id(conn, id)
            mlist.refresh_tags()

    conn.commit()
    conn.close()
