Your message to $name with subject
"${subject}"
has been held for moderation.

It will be delivered to the list recipients as soon as it has been
approved by a moderator.

If you wish to cancel the message without delivery, please click
this link:
${webroot}/manage/unqueue/${usertoken}/
