Your message to $name with subject
"${subject}"
has been released from moderation, and will be delivered to all list
members shortly.
