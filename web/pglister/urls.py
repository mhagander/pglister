from django.conf.urls import url, include
from django.urls import path
from django.contrib import admin
from django.conf import settings

from pglister.lists.views import Home, Manage, Unqueue, UnqueueToken, Subscribe, Unsubscribe
from pglister.lists.views import EditSub, UnsubscribeConfirm, UnsubscribeLink, Testmail
from pglister.lists.views import ViewMembership, UnsubscribeHelp
from pglister.lists.views_api import ListApi, ArchivesApi, SubscribersApi
from pglister.lists.views_addr import AddMail, ConfirmMail, DelMail, Resend, Blacklist
from pglister.lists.views_moderate import Moderate, ModerateMail, ModerateList, ModerateScript
from pglister.lists.views_moderate import BlacklistDomain
from pglister.lists.views_manage import ManageList, ManageSubscribers, ManageSubscribersUnsubscribe
from pglister.lists.views_manage import ManageBounces, ManageGlobalSubscribers, ManageThreadBlacklist

import pglister.auth

urlpatterns = [
    url(r'^$', Home.as_view()),
    url(r'^manage/$', Manage.as_view()),
    url(r'^manage/unqueue/(?P<modid>\d+)/$', Unqueue.as_view()),
    url(r'^manage/unqueue/(?P<usertoken>[a-z0-9]+)/$', UnqueueToken.as_view()),
    url(r'^manage/subscribe/$', Subscribe.as_view()),
    url(r'^manage/edit/(?P<pk>\d+)/$', EditSub.as_view()),
    url(r'^manage/members/(?P<listid>\d+)/$', ViewMembership.as_view()),
    url(r'^manage/unsubscribe/(?P<subid>\d+)-(?P<listid>\d+)/$', Unsubscribe.as_view()),
    url(r'^manage/testmail/(?P<subid>\d+)-(?P<listid>\d+)/$', Testmail.as_view()),
    url(r'^manage/addmail/$', AddMail.as_view()),
    url(r'^manage/delmail/(?P<subid>\d+)/$', DelMail.as_view()),
    url(r'^manage/resend/(?P<subid>\d+)/$', Resend.as_view()),
    url(r'^manage/blacklist/(?P<subid>\d+)/$', Blacklist.as_view()),
    url(r'^manage/confirmmail/(?P<token>[a-z0-9]+)/$', ConfirmMail.as_view()),

    # Moderation
    url(r'^moderate/$', Moderate.as_view()),
    url(r'^moderate/(?P<j>[a-z0-9-]+)\.js$', ModerateScript.as_view()),
    url(r'^moderate/(?P<token>[a-z0-9]+)/(?P<result>approve|whitelist|discard|preview)/$', ModerateMail.as_view()),
    url(r'^listmoderate/(?P<token>[a-z0-9]+)/(?P<result>approve|discard)/$', ModerateList.as_view()),
    url(r'^moderate/manage/(?P<id>[0-9]+)/$', ManageList.as_view()),
    url(r'^moderate/manage/(?P<id>[0-9]+)/view/$', ManageSubscribers.as_view()),
    url(r'^moderate/manage/(?P<id>[0-9]+)/view/unsub/$', ManageSubscribersUnsubscribe.as_view()),
    url(r'^moderate/manage/global/search/$', ManageGlobalSubscribers.as_view()),
    url(r'^moderate/manage/blackthreads/$', ManageThreadBlacklist.as_view()),
    url(r'^moderate/bounces/(?P<listid>\d+)-(?P<sub>\d+)/$', ManageBounces.as_view()),
    url(r'^moderate/blacklist_domains/$', BlacklistDomain.as_view()),

    # Unsubscribe (which does not authenticate)
    url(r'^unsubscribe/$', UnsubscribeHelp.as_view()),
    url(r'^unsub/(?P<listid>\d+)/(?P<token>[a-z0-9]+)/$', UnsubscribeLink.as_view()),
    url(r'^unsubscribe-confirm/(?P<token>[a-z0-9]+)/$', UnsubscribeConfirm.as_view()),

    # REST API calls
    url(r'^api/list/(?P<listname>[a-z0-9@\.-]+)/$', ListApi.as_view()),
    url(r'^api/subscribers/(?P<listname>[a-z0-9@\.-]+)/$', SubscribersApi.as_view()),
    url(r'^api/archive/(?P<servername>[a-zA-Z0-9]+)/(?P<op>[a-z]+)/$', ArchivesApi.as_view()),

    # Auth system integration
    url(r'^(?:accounts/)?login/?$', pglister.auth.login),
    url(r'^(?:accounts/)?logout/?$', pglister.auth.logout),
    url(r'^auth_receive/$', pglister.auth.auth_receive),
    url(r'^auth_api/$', pglister.auth.auth_api),


    url(r'^admin/', admin.site.urls),
]


if settings.DEBUG_TOOLBAR:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
