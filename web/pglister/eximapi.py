from subprocess import Popen, PIPE


def exim_queue_length():
    n = 0
    for x in Popen('/usr/bin/mailq', shell=False, stdout=PIPE, stderr=PIPE).communicate()[0].splitlines():
        if len(x) > 5 and not x.startswith('     '):
            n += 1
    return n


def exim_active_processes():
    n = 0
    for x in Popen("/bin/ps -axo args", shell=True, stdout=PIPE, stderr=PIPE).communicate()[0].splitlines():
        if x.startswith("/usr/sbin/exim4 -Mc"):
            n += 1
    return n
