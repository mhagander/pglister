Your subscription to {{list}} has been rejected by a moderator
and will not be completed.

The reason for rejection was:

{{reason|safe}}

