from django import forms


class DescribedCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    option_template_name = 'widgets/described_checkbox_select_multiple_option.html'

    def get_context(self, name, value, attrs):
        r = super().get_context(name, value, attrs)

        # Build a dict of the descriptions. We have to loop over the inside queryset
        # (somewhat breaking the abstraction), because looping over the choices themselves
        # will already cut it to a tuple of (id, name).
        descs = {t.id: t.description for t in self.choices.queryset}

        # XXX: doesn't really support option groups, just straight options!
        for og in r['widget']['optgroups']:
            og[1][0]['description'] = descs[int(og[1][0]['value'])]

        return r
