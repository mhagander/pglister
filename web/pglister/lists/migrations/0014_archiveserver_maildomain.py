# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0013_eliminatecc'),
    ]

    operations = [
        migrations.AddField(
            model_name='archiveserver',
            name='maildomain',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
