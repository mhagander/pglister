# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0006_onetoone_tokens'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='listsubscription',
            options={'ordering': ['list', 'subscriber']},
        ),
        migrations.AlterModelOptions(
            name='subscriberaddress',
            options={'ordering': ['email']},
        ),
    ]
