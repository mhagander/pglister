# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0019_address_token'),
    ]

    operations = [
        migrations.AddField(
            model_name='archiveserver',
            name='apikey',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
