# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0017_re_white_blacklist'),
    ]

    operations = [
        migrations.RunSQL("UPDATE lists_subscriberaddress SET email=lower(email) WHERE email!=lower(email)"),
        migrations.RunSQL("ALTER TABLE lists_subscriberaddress ADD CONSTRAINT email_must_be_lowercase CHECK (email=lower(email))"),
        migrations.RunSQL("CREATE UNIQUE INDEX lists_subscriberaddress_email_lower_key ON lists_subscriberaddress USING btree(lower(email))"),

        migrations.RunSQL("UPDATE lists_listwhitelist SET address=lower(address) WHERE address!=lower(address)"),
        migrations.RunSQL("ALTER TABLE lists_listwhitelist ADD CONSTRAINT email_must_be_lowercase CHECK (address=lower(address))"),

        migrations.RunSQL("UPDATE lists_globalwhitelist SET address=lower(address) WHERE address!=lower(address)"),
        migrations.RunSQL("ALTER TABLE lists_globalwhitelist ADD CONSTRAINT email_must_be_lowercase CHECK (address=lower(address))"),

        migrations.RunSQL("UPDATE lists_globalblacklist SET address=lower(address) WHERE address!=lower(address)"),
        migrations.RunSQL("ALTER TABLE lists_globalblacklist ADD CONSTRAINT email_must_be_lowercase CHECK (address=lower(address))"),

    ]
