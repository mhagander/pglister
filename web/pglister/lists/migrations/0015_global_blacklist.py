# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0014_archiveserver_maildomain'),
    ]

    operations = [
        migrations.CreateModel(
            name='GlobalBlacklist',
            fields=[
                ('address', models.EmailField(max_length=254, serialize=False, primary_key=True)),
            ],
            options={
                'ordering': ('address',),
            },
        ),
    ]
