# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0027_merge_migrations'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='longdesc',
            field=models.TextField(blank=True),
        ),
    ]
