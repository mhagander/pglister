# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0011_readonly_api'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='list',
            options={'ordering': ('domain__name', 'name')},
        ),
    ]
